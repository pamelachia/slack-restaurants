import os

from flask import Flask, request, jsonify
import json
import random
import requests

app = Flask(__name__)


def is_number(s):
    try:
        float(s)
        return True
    except ValueError:
        return False


def request_restaurants(coordinates, radius="5000"):
    req = requests.get(
        "https://places.cit.api.here.com/places/v1/discover/explore",
        {
            "app_id": os.environ.get('HERE_APP_ID'),
            "app_code": os.environ.get('HERE_APP_CODE'),
            "size": "100",
            "in": coordinates[0] + "," + coordinates[1] + ";r=" + radius
        },
    )

    if req.status_code == 200:
        return json.loads(req.content)


def get_random_restaurant():
    coordinates = request.values.get(
        "coordinates", request.args.get("coordinates"))
    radius = request.values.get("radius", request.args.get("radius", "5000"))

    if not radius.isdigit():
        raise Exception("Radius is not an integer")

    if not coordinates:
        raise Exception("Must select coordinates")

    coordinates = coordinates.split(",")
    if len(coordinates) is not 2:
        raise Exception("Incompatible coordinates")

    if not is_number(coordinates[0]) or not is_number(coordinates[1]):
        raise Exception("Coordinates weren't floats")

    restaurants = request_restaurants(coordinates)

    if not restaurants:
        raise Exception("Request failed")

    return random.choice(restaurants["results"]["items"])


@app.route("/restaurants", methods=['GET', 'POST'])
def restaurants():
    try:
        restaurant = get_random_restaurant()
    except Exception as error:
        return jsonify({"text": str(error)})

    # return jsonify(restaurant)
    return jsonify({
        "response_type": "in_channel",
        "text": "Try " + restaurant["title"] + ": https://maps.google.com/?q=" + str(restaurant["position"][0]) + "," + str(restaurant["position"][1])
    })
